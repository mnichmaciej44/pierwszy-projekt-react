import React from 'react';
import Loader from './loader';
import PostCard from './PostCard';


const unique = (arr) => {
    return arr.reduce((arr, curr) => { 
        if(!arr.includes(curr))arr.push(curr);

        return arr;
    } , [])

}

const fetchUser = (userId) => fetch(`https://jsonplaceholder.typicode.com/users/${userId}`).then(res => res.json());

const fetchUsers = (userIds) => Promise.all(userIds.map(userId => fetchUser(userId)));

const getUserName = (users,userId) => {
    
    let imie = users.map(user => {

        if(user.id === userId) return user.name;
    })
    return imie ? imie : "Gal Anonim";

}

class Posts extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data: null
        }
        props.userId ? this.userId = "?userId="+props.userId : this.userId = "";
    }

    componentDidMount(){
        let lposts;
        fetch(`https://jsonplaceholder.typicode.com/posts${this.userId}`)
        .then(response => response.json())
        .then(
            posts => {
                lposts = posts;
                return fetchUsers(unique(posts.map(post => post.userId)))
            })
        .then(
            users => {
                lposts.map(post => {
                    post.userId = getUserName(users,post.userId);
                });
                this.setState( {data:lposts} );
            }
        );
    }


    render(){
        return(
            <div>
                {this.userId ? <div className="ui large header">Posts</div> : <div className="ui huge header">Posts</div>}
                <div className="ui grid">
                {this.state.data && this.state.data.length > 1 ? this.state.data.map(post => <div key={post.id} className="four wide column"><PostCard  id={post.id} userId={post.userId} title={post.title} body={post.body} /></div>)
                : this.state.data ? <div key={this.state.data.id} className="four wide column"><PostCard  id={this.state.data.id} userId={this.state.data.userId} title={this.state.data.title} body={this.state.data.body} /></div> : <Loader/>}
                </div>
            </div>
            
            )
    }



}
export default Posts;