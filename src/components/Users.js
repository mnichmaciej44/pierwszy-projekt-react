import React from 'react';
import Loader from './loader';
import UserCard from './UserCard';


class Users extends React.Component{
    constructor(props) {
        super(props);
    
        this.state = {
          data: null,
        };
        props.id ? this.userid = "/"+props.id : this.userid = "";
       
      }

      componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users'+this.userid)
          .then(response => response.json())
          .then(data => this.setState({ data }));
      }
      
      


    render(){
       
        return(
            <div>
                <div className="ui huge header">Users</div>
                <div className="ui items">
                     {this.state.data && this.state.data.length > 1 ?
                         this.state.data.map(
                             user => <UserCard key={user.id} id={user.id} name={user.name} company={user.company} email={user.email}/>
                             ) :
                             this.state.data ? <UserCard key={this.state.data.id} id={this.state.data.id} name={this.state.data.name} company={this.state.data.company} email={this.state.data.email}/>: <Loader/>}
                </div>
            </div>
            )
    }



}
export default Users;