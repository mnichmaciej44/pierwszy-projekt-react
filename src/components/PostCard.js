import React from 'react';
import {Link} from 'react-router-dom';

const PostCard = (props) =>{

    return(
    
        <div className="ui cards postcard">
            <div className="card">
                <div className="content">
                <div className="header">{props.title}</div>
                <div className="meta"><Link to={"/users/"+props.userId}>{props.userId}</Link></div>
                <div className="description">
                {props.body}
                </div>
                </div>
            </div>
        </div>

        )

}

export default PostCard;