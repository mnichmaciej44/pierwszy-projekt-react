import React from 'react';
import {Link} from 'react-router-dom';

const UserCard = (props) =>{

    return(
    <Link to={"/users/"+props.id} className="uiCarditem link" >
        <div className="ui segment useritem">
        <div className="ui item ">
            <div className="content">
                <span className="header">{props.name}</span>
                <div className="meta">
                    <p className="bold">Description</p>
                </div>
            <div className="description">
                <p><span className="bold">Company name: </span>{props.company.name}</p>
                <p><span className="bold">Catchphrase: </span>{props.company.catchPhrase}</p>
                <p><span className="bold">bs: </span>{props.company.bs}</p>
             </div>
            <div className="extra">
            <span className="bold">Email: </span>{props.email}
            </div>
        </div>
        </div>
        </div>
    </Link>

        )

}

export default UserCard;