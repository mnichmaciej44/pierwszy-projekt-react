import React from 'react';

const Loader = () =>{
return(
    <div className="ui active inline loader">
    <div className="ui text loader">Loading</div>
    </div>
)
}

export default Loader;