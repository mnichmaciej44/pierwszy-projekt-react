import React from 'react';
import Users from './Users';
import Posts from './Posts';


const User = (props) =>{

    return(
        <div>
        <Users id={props.id}/>
        <Posts userId={props.id}/>
        </div>
        )

}

export default User;