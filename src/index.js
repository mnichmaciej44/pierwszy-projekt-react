import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';

import "./index.scss";
import NavBar from './components/NavBar';
import Home from './components/Home';
import Posts from './components/Posts';
import Users from './components/Users';
import User from './components/User';



const users = () =>{
    return <Users/>
}

const posts = () =>{
    return <Posts/>
}
const home = () =>{
    return <Home/>
}
const user = (path) =>{
     return <User id={path.match.params.userId}/>
    
}


class App extends React.Component{



    render(){
        return(
            <div>
                
                <BrowserRouter>
                    <div>
                        <NavBar/>
                        <div className="pagecontent">
                            <Route path="/" exact component={home}/>
                            <Route path="/users" exact component={users}/>
                            <Route path="/users/:userId" component={user}/>
                            <Route path="/posts" exact component={posts}/>
                        </div>
                    </div>
                </BrowserRouter>
            </div>
        )
    }

}


ReactDOM.render(<App/>, document.querySelector('#root'));